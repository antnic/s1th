#ifndef __PORTAUDIO_SOUND_HEADER__
#define __PORTAUDIO_SOUND_HEADER__

#include "portaudio.h"
#include "../hw_config.h"
#include <queue>
#include <cstddef>
#include <iostream>
#include "../sound/sound_generator.h"


class Sound_PortAudio
{

	public:
		Sound_PortAudio();
		~Sound_PortAudio();


		Sound_Generator *sg;

		std::queue<float>* get_output_buffer_pointer();
		int* get_number_of_samples_pointer();

		void initialize_stream();

		int pa_class_callback(const void *input, void *output,
							unsigned long frameCount,
							const PaStreamCallbackTimeInfo* timeInfo,
							PaStreamCallbackFlags statusFlags);

		static int myPaCallback(const void *input, void *output,
								unsigned long frameCount,
								const PaStreamCallbackTimeInfo* timeInfo,
								PaStreamCallbackFlags statusFlags,
								void *userData )
		{return ((Sound_PortAudio*)userData)
		     ->pa_class_callback(input, output, frameCount, timeInfo, statusFlags);}


		SOUND_OUTPUT* input_;

		float convert(signed int i );

	private:


		PaStreamParameters outputParameters;
		PaStream *stream_;
		PaError err_;

private:



};



#endif
