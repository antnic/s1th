
#include "pa.h"

Sound_PortAudio::Sound_PortAudio()
{
	//this->number_of_samples_=0;
}

void Sound_PortAudio::initialize_stream()
{
	this->err_ = Pa_Initialize();
	    //if( err != paNoError ) goto error;

	    this->outputParameters.device = Pa_GetDefaultOutputDevice(); /* default output device */
	    if (this->outputParameters.device == paNoDevice) {
	      //fprintf(stderr,"Error: No default output device.\n");
	      //goto error;
	    }
	    this->outputParameters.channelCount = 1;       /* stereo output */
	    this->outputParameters.sampleFormat = paFloat32; /* 32 bit floating point output */
	    this->outputParameters.suggestedLatency = Pa_GetDeviceInfo( this->outputParameters.device )->defaultLowOutputLatency;
	    this->outputParameters.hostApiSpecificStreamInfo = NULL;


	    this->err_ = Pa_OpenStream(
	              &(this->stream_),
	              NULL, /* no input */
	              &this->outputParameters,
	              SAMPLE_RATE,
	              FRAMES_PER_BUFFER,
	              paClipOff,      /* we won't output out of range samples so don't bother clipping them */
	              &Sound_PortAudio::myPaCallback,
	              this);
	    //if( err != paNoError ) goto error;

	    std::cout <<"PortAudio Error :" << this->err_ << std::endl;

	    Pa_StartStream( this->stream_ );
}

//std::queue<float>* Sound_PortAudio::get_output_buffer_pointer(){return &(this->output_buffer_);}
//int* Sound_PortAudio::get_number_of_samples_pointer(){return &(this->number_of_samples_);}

int Sound_PortAudio::pa_class_callback(const void *input, void *output,
  unsigned long frameCount,
  const PaStreamCallbackTimeInfo* timeInfo,
  PaStreamCallbackFlags statusFlags)
{


	int i;

	float *out = (float*)output;
	(void) input; /* Prevent "unused variable" warnings. */

	int s[frameCount];

	float sound = 0;

	//clear normalization variables
	this->sg->clear_samples_history();

	//for( i=0; i<frameCount; i++ )
	//{

		//s[i] =
	//}


	for( i=0; i<frameCount; i++ )
	{

			//sound = s[i] / (sg->get_max_amplitude()/INT_LENGTH);
		sound = this->sg->generate_sound();;


		//convert to -1 +1
		sound = this->convert(sound);



		*out++ = sound;
		//*out++ = sound;
	}

  return paContinue;
}

float Sound_PortAudio::convert(signed int i )
{
	float f;
	f = ((float) i) / (float) ((INT_LENGTH<<4)+0b00000111);
	if( f > 1 ) f = 1;
	if( f < -1 ) f = -1;

	return f;
}
