
#ifndef __MIX_ADDITIVE_HEADER__
#define __MIX_ADDITIVE_HEADER__

#include "../hw_config.h"
//#include "mixer_abstr.h"
#include "../waveforms/wave_abstr.h"

#include <vector>
#include <queue>
#include <pthread.h>
#include <unistd.h>
#include <iostream>

typedef struct mix_param_struct
{
	SOUND_OUTPUT* input;
	float gain;
} MIXER_INPUT;


class Mixer //: public Mixer
{

public:

	Mixer();
	~Mixer();
	void mix_input();

	std::vector<MIXER_INPUT> input_list_;
	SOUND_OUTPUT output_;

	SOUND_OUTPUT* get_output_pointer();
	void set_input(std::vector<MIXER_INPUT> input);

	inline double get_mixed_sample()
	{

		float out_value=0;

		for(int i=0;i<this->input_list_.size();i++)
		{
			if(this->input_list_[i].input->samples_number !=0)
			{
				out_value += (this->input_list_[i]).input->samples_buffer->get()
					* (this->input_list_[i]).gain *((float)1/this->input_list_.size());
				this->input_list_[i].input->samples_number --;

			}
		//std::cout << out_value << std::endl;
		}
		return out_value;

	}

};

#endif /* __MIX_ADDITIVE_HEADER__ */
