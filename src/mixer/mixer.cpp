/*
 * mix_additive.cpp
 *
 *  Created on: 28 déc. 2013
 *      Author: nicolas
 */

#include "mixer.h"


Mixer::Mixer()
{

	this->output_.samples_buffer = new LockFreeBuffer<double>(BUFFER_SIZE,"mixer output");
	this->output_.samples_number =0;

}

Mixer::~Mixer(){}

void Mixer::mix_input()
{

	int i=0,t=0;

	while(this->output_.samples_number< TABLE_SIZE)
	{
			float out_value=0;
			for(i=0;i<this->input_list_.size();i++)
			{
				MIXER_INPUT* tmp_mix = &(this->input_list_[i]);

				if(!(tmp_mix->input->samples_number == 0))
				{
					out_value += tmp_mix->input->samples_buffer->get() * tmp_mix->gain *((float)1/this->input_list_.size());
					tmp_mix->input->samples_number = tmp_mix->input->samples_number -1;
				}
			}

			//this->output_.samples_buffer.push(out_value);
			this->output_.samples_buffer->put(out_value);
			this->output_.samples_number = this->output_.samples_number + 1;
	}
}

void Mixer::set_input(std::vector<MIXER_INPUT> input)
{
	this->input_list_ = input;
}

SOUND_OUTPUT* Mixer::get_output_pointer()
{
	return &(this->output_);
}

