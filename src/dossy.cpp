
#include "hw_config.h"

#include "audio/pa.h"
#include "interface/midi.h"

#include "sound/sound_generator.h"

#include <stdio.h>

//#include <iostream>
//#include <unistd.h>
#include <pthread.h>


/*******************************************************************/
int main(void)
{

	int i;

	//Midi Event
	Midi* midi = new Midi();

	//Synth Voice
	Sound_Generator *s1 = new Sound_Generator();

	//Audio Engine
	Sound_PortAudio* sp = new Sound_PortAudio();

	std::cout << "Port Audio address : " << sp->input_ << std::endl;

	s1->set_keyboard_input(midi->get_keyboard_struct_pointer());
	sp->sg = s1;

	midi->start_midi_tread();
	//s1->start_thread();

	sleep(1);

	sp->initialize_stream();

	//Pause
	std::cin >>i;

return 0;
}
