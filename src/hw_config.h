#ifndef __HW_CONFIG__
#define __HW_CONFIG__

#define SAMPLE_RATE 44100
#define TABLE_SIZE  912//628 //672 // 720 //768 // 816 // 864 //912
#define BUFFER_SIZE 4096
#define FRAMES_PER_BUFFER  (128)
#define GEN_WAITING_TIME 5000

//#define SIZE 1024
// oscillator
#define overSamp (2)        /* oversampling factor (positive integer) */
#define baseFrequency (20)  /* starting frequency of first table */
#define constantRatioLimit (99999)    /* set to a large number (greater than or equal to the length of the lowest octave table) for constant table*/

#define INT_LENGTH 0xFFFF

#define MAX_POLY 64

#ifndef M_PI
	#define M_PI  (3.14159265)
#endif

#include "LockFreeBuffer.h"

typedef struct _key
{
	float frequency;
	int is_active;
	int note;
} KEYBOARD_STRUCT;

typedef struct _sound_output
{
	LockFreeBuffer<double>* samples_buffer;
	int samples_number;
} SOUND_OUTPUT;



#endif
