/*
 * periodic.h
 *
 *  Created on: 29 déc. 2013
 *      Author: nicolas
 */

#ifndef __PERIODIC_HEADER__
#define __PERIODIC_HEADER__

struct periodic_info
{
	int timer_fd;
	unsigned long long wakeups_missed;
};

int make_periodic (unsigned int period, struct periodic_info *info);

void wait_period (struct periodic_info *info);





#endif /* __PERIODIC_HEADER__ */
