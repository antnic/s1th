#ifndef __MIDI_HEADER__
#define __MIDI_HEADER__

//MIDI SEQUENCER
#define __LINUX_ALSA__
#define MIDI_PORT 1

#include "../hw_config.h"

#include <sys/soundcard.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <pthread.h>
#include <cstdlib>
#include <signal.h>
#include <math.h>
#include <vector>

#include "RtMidi.h"

typedef enum _midi_message
{
	NOTE_ON = 0b1001,
	NOTE_OFF = 0b1000
} MIDI_MESSAGE;

class Midi
{

public:

	Midi();

	void start_midi_tread();
	static void* run_midi_thread(void* p);

	std::vector<KEYBOARD_STRUCT>* get_keyboard_struct_pointer();

	void add_note(int note);
		void remove_note(int note);

private:

	KEYBOARD_STRUCT key;
	int sequencer_file_descriptor;
	unsigned char inbytes[4];         // bytes from sequencer driver
	int status;               // for error checking
	pthread_t midi_tread;

	RtMidiIn *midi_in;
	bool done;

	std::vector<unsigned char> message;
	int nBytes, i;
	double stamp;

	std::vector<KEYBOARD_STRUCT> note_list_;

	float note_to_frequency(unsigned char note);
};




#endif
