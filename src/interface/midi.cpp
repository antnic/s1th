#include "midi.h"


Midi::Midi()
{

	this->key.frequency = 440;
	this->key.is_active = 0;

	midi_in = new RtMidiIn();

	unsigned int nPorts = midi_in->getPortCount();

	if ( nPorts == 0 )
	{
		std::cout << "No ports available!\n";
	//ERREUR
	}
	midi_in->openPort(MIDI_PORT);

	std::cout << midi_in->getPortName(0) << std::endl;
	std::cout << midi_in->getPortName(1) << std::endl;

	// Don't ignore sysex, timing, or active sensing messages.
	//std::cout << "Fin" << std::endl;
	midi_in->ignoreTypes( false, false, false );


	//std::cout << "Fin" << std::endl;
	// Install an interrupt handler function.
//	done = false;
	//(void) signal(SIGINT, finish);

}


void* Midi::run_midi_thread(void* p)
{

	Midi* midi_obj = (Midi*)p;

	int i;

	  while ( !(midi_obj->done) )
	  {
	    midi_obj->stamp = midi_obj->midi_in->getMessage( &(midi_obj->message) );
	    midi_obj->nBytes = midi_obj->message.size();
	    //for ( i=0; i<midi_obj->nBytes; i++ )
//	      std::cout << "Byte " << i << " = " << (int)midi_obj->message[i] << ", ";
	    if ( midi_obj->nBytes > 0 )
	    {
	//    	 std::cout << "stamp = " << midi_obj->stamp << std::endl;
	    	 //std::cout << (midi_obj->message[0] >> 4) << std::endl;

	    	if((midi_obj->message[0] >> 4) == NOTE_ON)
	    	{
	    		if(midi_obj->message[2] != 0)
	    			midi_obj->add_note(midi_obj->message[1]);
	    		else
	    			midi_obj->remove_note(midi_obj->message[1]);
	    		std::cout << "NOTE ON" << std::endl;
	    	}
	    	else if((midi_obj->message[0] >> 4) == NOTE_OFF)
	    	{
	    		midi_obj->remove_note(midi_obj->message[1]);
		    	std::cout << "NOTE OFF" << std::endl;
	    	}
	    }

	    usleep( 500 );
	  }
}

void Midi::start_midi_tread()
{
	status = pthread_create(&(this->midi_tread), NULL, Midi::run_midi_thread, (void*)this);
}

float Midi::note_to_frequency(unsigned char note)
{
	float tmp = (note - 69);
	tmp /=12;
	return (pow(2,tmp)*440);
}

std::vector<KEYBOARD_STRUCT>* Midi::get_keyboard_struct_pointer()
{
	return &(this->note_list_);
}

void Midi::add_note(int note)
{

	int i;

	for(i=0;i<this->note_list_.size();i++)
	{
		if(this->note_list_.at(i).note == note)
			return;
	}

	KEYBOARD_STRUCT k;
	k.note = note;k.is_active = 1;k.frequency = this->note_to_frequency(note);

	std::cout << k.note << " " << k.frequency << std::endl;

	this->note_list_.push_back(k);

}

void Midi::remove_note(int note)
{
	int i;

	for(i=0;i<this->note_list_.size();i++)
	{
		//if(this->note_list_.at(i).note == note)
		if(this->note_list_[i].note == note)
			this->note_list_.erase(this->note_list_.begin()+i);
	}

}

