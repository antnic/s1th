#include "sound_generator.h"
#include <unistd.h>


Sound_Generator::Sound_Generator()
{
	KEYBOARD_STRUCT k;
	k.frequency = 440.0f;
	this->last_key_=k;



	Waveforms *w1 = new Saw();
	w1->set_detune(0);
	this->oscillator_.push_back(w1);

	Waveforms *w2 = new Saw();
	w2->set_detune(0.02);
	this->oscillator_.push_back(w2);

	Waveforms *w3 = new Saw();
	w3->set_detune(-0.02);
	this->oscillator_.push_back(w3);

	Waveforms *w4 = new Saw();
	w4->set_detune(-0.006);
	this->oscillator_.push_back(w4);

	Waveforms *w5 = new Saw();
	w5->set_detune(+0.006);
	this->oscillator_.push_back(w5);

	Waveforms *w6 = new Saw();
	w6->set_detune(0.013);
	this->oscillator_.push_back(w6);

	Waveforms *w7 = new Saw();
	w7->set_detune(-0.013);
	this->oscillator_.push_back(w7);


	this->is_polyphonic_ = 1;
	this->last_key_count_=0;
	this->poly_count=0;

	this->lpFilter_ = new Biquad();
	this->lpFilter_->setBiquad(bq_type_lowpass, 20000.0f / SAMPLE_RATE, 0.707, 1);

	this->env_ = new ADSR();

	// initialize settings
	this->env_->setAttackRate(0.01 * SAMPLE_RATE);  // .1 second
	this->env_->setDecayRate(.1 * SAMPLE_RATE);
	this->env_->setReleaseRate(0.01 * SAMPLE_RATE);
	this->env_->setSustainLevel(0.99);

}

signed int Sound_Generator::generate_sound()
	{
		int i,j;
		signed int tmp_value=0;




			//key pressed
			if(!(this->keyboard_input_->empty()))
			{
				if(!this->is_polyphonic_)
					this->last_key_ = this->keyboard_input_->back();

				else //if(this->keyboard_input_->size() != this->last_key_count_)
				{
					this->clear_poly_flag();
					//copy all oscillator
					//ToDO a l 	'envers pour avoir les dernière note en priorité
					for(j=0;j<this->keyboard_input_->size();j++)
						this->add_poly_voice(this->keyboard_input_->at(j).frequency);

					this->last_key_count_ = this->keyboard_input_->size();
				}
			this->env_->gate(true);
		}
		//no key pressed
		//ToDo Release Polyphonic
		else
		{

			this->clear_poly_flag();

			this->env_->gate(false);
			this->last_key_count_=0;
		}



		this->remove_poly_voice(NULL);
		tmp_value=0;
		if(!this->is_polyphonic_)
		{
			for(i=0;i<this->oscillator_.size();i++)
			{
				this->oscillator_[i]->wavetable_->setFrequency((this->last_key_.frequency*this->oscillator_[i]->frequency_multiplier_) / SAMPLE_RATE);
				tmp_value += this->oscillator_[i]->gen_waveform() ;//* ((float)1/this->oscillator_.size());
			}
		}
		else
		{
			for(i=0;i<this->poly_osc_list_.size();i++)
			{
				for(j=0;j<this->poly_osc_list_[i].osc_voice.size();j++)
					tmp_value += this->poly_osc_list_[i].osc_voice[j].gen_waveform();
			}
		}

		tmp_value = this->lpFilter_->process(tmp_value);
		tmp_value = tmp_value * this->env_->process();

		//update normalization variable
		if(std::abs(tmp_value) > this->max_amplitude_)
			this->max_amplitude_ = std::abs(tmp_value);
		this->generated_samples_++;

		return tmp_value;
	}


void Sound_Generator::add_poly_voice(float frequency)
{
	for(int i=0;i<this->poly_osc_list_.size();i++)
	{
		//check if note is in the list
		if(this->poly_osc_list_[i].frequency == frequency)
		{
			this->poly_osc_list_[i].exist = 1;
			return;
		}
	}
		//add the note to the list
		POLY_STRUCT p_temp;
		p_temp.exist = 1;
		p_temp.frequency = frequency;
		for(int j=0;j<this->oscillator_.size();j++)
		{
				Waveforms w = this->oscillator_[j]->clone(NULL);
				w.set_frequency(frequency);
				p_temp.osc_voice.push_back(w);
		}
		this->poly_osc_list_.push_back(p_temp);
}


void Sound_Generator::remove_poly_voice(Waveforms* w)
{
	for(int i=0;i<this->poly_osc_list_.size();i++)
	{
		if(!(this->poly_osc_list_[i].exist))
			this->poly_osc_list_.erase(poly_osc_list_.begin() + i);
	}
}

void Sound_Generator::clear_poly_flag()
{
	for(int i=0;i<this->poly_osc_list_.size();i++)
		this->poly_osc_list_[i].exist = 0;
}



