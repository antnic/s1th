
/***
 * @file sound_generator.h
 * @brief Synth voice header file
 *
 */

#ifndef __SOUND_GENERATOR_HEADER__
#define __SOUND_GENERATOR_HEADER__


#include "../hw_config.h"
#include "../waveforms/wave_abstr.h"
#include "../waveforms/wave_square.h"
#include "../waveforms/wave_sin.h"
#include "../waveforms/wave_saw.h"
#include "../periodic.h"
#include "../mixer/mixer.h"
#include "../filter/filter.h"
#include "../env/ADSR.h"

#include <vector>
#include <pthread.h>
#include <cmath>

typedef struct poly_struct_
{
	double frequency;
	int note;
	std::vector<Waveforms> osc_voice;
	int exist;

} POLY_STRUCT;


class Sound_Generator
{

public:

	Sound_Generator();
	~Sound_Generator();

	void set_keyboard_input(std::vector<KEYBOARD_STRUCT> *key_input){ this->keyboard_input_ = key_input;}

	signed int generate_sound();

	void set_oscillator();

	void clear_samples_history(){this->max_amplitude_=0;this->generated_samples_=0;this->min_amplitude_=0;}
	int get_max_amplitude(){return this->max_amplitude_;}
	int get_min_amplitude(){return this->min_amplitude_;}
	int get_generated_samples(){return this->generated_samples_;}


	void add_poly_voice(float frequency);
	void remove_poly_voice(Waveforms* w);
	void clear_poly_flag();


private:

	int max_amplitude_;
	int min_amplitude_;
	int generated_samples_;

	//Voice Sound Generator Note Input
	std::vector<KEYBOARD_STRUCT>* keyboard_input_;
	KEYBOARD_STRUCT last_key_;

	std::vector<POLY_STRUCT> poly_osc_list_;

	int last_key_count_;
	int poly_count;

	//Test Code EarLevel
	Biquad *lpFilter_;
	ADSR *env_;

	//Oscillators
	std::vector<Waveforms*> oscillator_;
	Mixer* osc_mix_;

	//Polyphony
	bool is_polyphonic_;
	Mixer* poly_mix_;

	//Portamento
	bool glide_active;
	bool glide_legato;
	int glide_length;

};


#endif /* SOUND_GENERATOR_H_ */
