
#include "wave_abstr.h"


/*Oscillator::Oscillator()
{
	this->frequency_multiplier_ = 1;
	this->detune = 0;
	this->alpha=0;
}

Oscillator::~Oscillator()
{

}*/

void Waveforms::set_frequency_multiplier(float multiplier) { this->frequency_multiplier_ = multiplier; }
float Waveforms::get_frequency_multiplier(){ return this->frequency_multiplier_;}

void Waveforms::set_detune(float detune){ this->detune_ = detune;}
float Waveforms::get_detune(){ return this->detune_;}

void Waveforms::set_alpha(float alpha){this->alpha_= alpha;}
float Waveforms::get_alpha(){return this->alpha_;}



//
// if scale is 0, auto-scales
// returns scaling factor (0.0 if failure), and wavetable in ai array
//
float Waveforms::makeWaveTable(WaveTableOsc *osc, int len, double *ar, double *ai, double scale, double topFreq) {
    fft(len, ar, ai);

    if (scale == 0.0) {
        // calc normal
        double max = 0;
        for (int idx = 0; idx < len; idx++) {
            double temp = fabs(ai[idx]);
            if (max < temp)
                max = temp;
        }
        scale = 1.0 / max * .999;
    }

    // normalize
    signed int wave[len];
    for (int idx = 0; idx < len; idx++)
    {
    	//std::cout << ai[idx]*scale*INT_LENGTH << std::endl;
    	wave[idx] = ai[idx] * scale*(INT_LENGTH >> 1);
    }


    if (osc->addWaveTable(len, wave, topFreq))
        scale = 0.0;

    return scale;
}

//
// fft
//
// I grabbed (and slightly modified) this Rabiner & Gold translation...
//
// (could modify for real data, could use a template version, blah blah--just keeping it short)
//
void Waveforms::fft(int N, double *ar, double *ai)
/*
 in-place complex fft

 After Cooley, Lewis, and Welch; from Rabiner & Gold (1975)

 program adapted from FORTRAN
 by K. Steiglitz  (ken@princeton.edu)
 Computer Science Dept.
 Princeton University 08544          */
{
    int i, j, k, L;            /* indexes */
    int M, TEMP, LE, LE1, ip;  /* M = log N */
    int NV2, NM1;
    double t;               /* temp */
    double Ur, Ui, Wr, Wi, Tr, Ti;
    double Ur_old;

    // if ((N > 1) && !(N & (N - 1)))   // make sure we have a power of 2

    NV2 = N >> 1;
    NM1 = N - 1;
    TEMP = N; /* get M = log N */
    M = 0;
    while (TEMP >>= 1) ++M;

    /* shuffle */
    j = 1;
    for (i = 1; i <= NM1; i++) {
        if(i<j) {             /* swap a[i] and a[j] */
            t = ar[j-1];
            ar[j-1] = ar[i-1];
            ar[i-1] = t;
            t = ai[j-1];
            ai[j-1] = ai[i-1];
            ai[i-1] = t;
        }

        k = NV2;             /* bit-reversed counter */
        while(k < j) {
            j -= k;
            k /= 2;
        }

        j += k;
    }

    LE = 1.;
    for (L = 1; L <= M; L++) {            // stage L
        LE1 = LE;                         // (LE1 = LE/2)
        LE *= 2;                          // (LE = 2^L)
        Ur = 1.0;
        Ui = 0.;
        Wr = cos(M_PI/(float)LE1);
        Wi = -sin(M_PI/(float)LE1); // Cooley, Lewis, and Welch have "+" here
        for (j = 1; j <= LE1; j++) {
            for (i = j; i <= N; i += LE) { // butterfly
                ip = i+LE1;
                Tr = ar[ip-1] * Ur - ai[ip-1] * Ui;
                Ti = ar[ip-1] * Ui + ai[ip-1] * Ur;
                ar[ip-1] = ar[i-1] - Tr;
                ai[ip-1] = ai[i-1] - Ti;
                ar[i-1]  = ar[i-1] + Tr;
                ai[i-1]  = ai[i-1] + Ti;
            }
            Ur_old = Ur;
            Ur = Ur_old * Wr - Ui * Wi;
            Ui = Ur_old * Wi + Ui * Wr;
        }
    }
}

Waveforms Waveforms::clone (Waveforms* other)
	{

		Waveforms w;

		//copy value
		w.alpha_ = this->alpha_;
		w.detune_ = this->detune_;
		w.frequency_multiplier_ = this->frequency_multiplier_;
		w.voices_detune_ = this->voices_detune_;
		w.voices_number_ = this->voices_number_;
		w.voices_spread_ = this->voices_spread_;

		w.wavetable_ = new WaveTableOsc();
		w.wavetable_->clone(this->wavetable_);

		return w;
		}

