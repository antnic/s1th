#ifndef __WAVE_SAW_HEADER__
#define __WAVE_SAW_HEADER__

#include "wave_abstr.h"

class Saw : public Waveforms {


public:
	Saw();

private:
	void setSawtoothOsc(WaveTableOsc *osc, float baseFreq);
	void defineSawtooth(int len, int numHarmonics, double *ar, double *ai);

};
#endif
