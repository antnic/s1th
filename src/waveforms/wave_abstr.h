#ifndef __WAVE_ABSTRACT_HEADER__
#define __WAVE_ABSTRACT_HEADER__

#include "../hw_config.h"
#include "../periodic.h"
#include "WaveTableOsc.h"
#include <queue>
#include <iostream>
#include <pthread.h>
#include <unistd.h>
#include <math.h>

class Waveforms
{


public:

	//Oscillator();
	//~Oscillator();
	inline virtual double gen_waveform()
	{
		 signed int value = this->wavetable_->getOutput(); //* gainMult;
		 this->wavetable_->updatePhase();
		 return value;
	}

	Waveforms clone (Waveforms* other);

	void set_frequency_multiplier(float multiplier);
	float get_frequency_multiplier();

	void set_detune(float detune);
	float get_detune();

	void set_alpha(float alpha);
	float get_alpha();


	WaveTableOsc* wavetable_;
	float frequency_multiplier_;


	float get_frequency(){ return this->frequency_; }

	void set_frequency(double freq)
	{
		this->frequency_ = freq;
		float detune_mult = (float)1+this->detune_;
		std::cout << this->detune_ << " " << detune_mult << std::endl;
		this->wavetable_->setFrequency(((this->frequency_/**this->frequency_multiplier_*/)*detune_mult)/SAMPLE_RATE);
	}

protected:

	float frequency_;
	float detune_;
	float alpha_;

	SOUND_OUTPUT output_;


	int voices_number_;
	int voices_detune_;
	int voices_spread_;

	float makeWaveTable(WaveTableOsc *osc, int len, double *ar, double *ai, double scale, double topFreq);
	void fft(int N, double *ar, double *ai);

	//int clone_source_pointer_;

};

#endif
