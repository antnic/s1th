#ifndef __WAVE_SQUARE_HEADER__
#define __WAVE_SQUARE_HEADER__

#include "wave_abstr.h"

class Square : public Waveforms
{

public:
	Square();

	virtual double gen_waveform();

private:
	void setSawtoothOsc(WaveTableOsc *osc, float baseFreq);
	void defineSawtooth(int len, int numHarmonics, double *ar, double *ai);
};
#endif
