
#include "wave_sin.h"



Sinus::Sinus()
{
	this->frequency_multiplier_=1;
	this->alpha_=0;
	this->detune_=0;
	this->output_.samples_buffer = new LockFreeBuffer<double>(BUFFER_SIZE,"sinus osc");
	this->output_.samples_number = 0;
	this->wavetable_ = new WaveTableOsc();

    const int sineTableLen = 2048;
    signed int sineTable[sineTableLen];
    for (int idx = 0; idx < sineTableLen; ++idx)
        sineTable[idx] = sin((float)idx / sineTableLen * M_PI * 2)*INT_LENGTH;
    this->wavetable_->addWaveTable(sineTableLen, sineTable, 16000.0);
}



