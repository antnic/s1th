
#include "wave_saw.h"



Saw::Saw()
{
	this->alpha_=0;
	this->detune_=0;
	this->frequency_multiplier_=1;
	this->output_.samples_buffer = new LockFreeBuffer<double>(BUFFER_SIZE,"saw osc");
	this->output_.samples_number = 0;

	this->wavetable_ = new WaveTableOsc();
	setSawtoothOsc(this->wavetable_, baseFrequency);

}

//
// setSawtoothOsc
//
// make set of wavetables for sawtooth oscillator
//
void Saw::setSawtoothOsc(WaveTableOsc *osc, float baseFreq) {

    // calc number of harmonics where the highest harmonic baseFreq and lowest alias an octave higher would meet
    int maxHarms = SAMPLE_RATE / (3.0 * baseFreq) + 0.5;

    // round up to nearest power of two
    unsigned int v = maxHarms;
    v--;            // so we don't go up if already a power of 2
    v |= v >> 1;    // roll the highest bit into all lower bits...
    v |= v >> 2;
    v |= v >> 4;
    v |= v >> 8;
    v |= v >> 16;
    v++;            // and increment to power of 2
    int tableLen = v * 2 * overSamp;  // double for the sample rate, then oversampling

    double ar[tableLen], ai[tableLen];   // for ifft

    double topFreq = baseFreq * 2.0 / SAMPLE_RATE;
    double scale = 0.0;
    for (; maxHarms >= 1; maxHarms >>= 1)
    {
        defineSawtooth(tableLen, maxHarms, ar, ai);
        scale = makeWaveTable(osc, tableLen, ar, ai, scale, topFreq);
        topFreq *= 2;
        if (tableLen > constantRatioLimit) // variable table size (constant oversampling but with minimum table size)
            tableLen >>= 1;
    }
}

void Saw::defineSawtooth(int len, int numHarmonics, double *ar, double *ai) {
    if (numHarmonics > (len >> 1))
        numHarmonics = (len >> 1);

    // clear
    for (int idx = 0; idx < len; idx++) {
        ai[idx] = 0;
        ar[idx] = 0;
    }

    // sawtooth
    /**/for (int idx = 1, jdx = len - 1; idx <= numHarmonics; idx++, jdx--) {
        double temp = (-1.0 / idx);//*INT_LENGTH;
        ar[idx] = -temp;
        ar[jdx] = temp;
    }

    // examples of other waves

    /* // square
     for (int idx = 1, jdx = len - 1; idx <= numHarmonics; idx++, jdx--) {
     double temp = idx & 0x01 ? 1.0 / idx : 0.0;
     ar[idx] = -temp;
     ar[jdx] = temp;
     }
     */
    /*
     // triangle
     float sign = 1;
     for (int idx = 1, jdx = len - 1; idx <= numHarmonics; idx++, jdx--) {
     double temp = idx & 0x01 ? 1.0 / (idx * idx) * (sign = -sign) : 0.0;
     ar[idx] = -temp;
     ar[jdx] = temp;
     }
     */
}

