/*
 * LockFreeBuffer.h
 *
 *  Created on: 10 janv. 2014
 *      Author: nicolas
 */

#ifndef __LOCKFREEBUFFER_HEADER__
#define __LOCKFREEBUFFER_HEADER__

#include <vector>
#include <exception>
#include <iostream>



using std::vector;
using std::exception;

template<class T> class LockFreeBuffer
{
public:
	//LockFreeBuffer (unsigned bufsz) : readidx(0), writeidx(0), buffer(bufsz)
	//{}

	LockFreeBuffer (unsigned bufsz,const char* name) : readidx(0), writeidx(0), buffer(bufsz),name(name)
	{}

	T get (void)
	{
		if (readidx == writeidx)
		{
			std::cerr << "underrun " << name << std::endl;
			throw exception();
		}

		T result = buffer[readidx];

		if ((readidx + 1) >= buffer.size())
			readidx = 0;
		else
			readidx = readidx + 1;

		return result;
	}

	void put (T datum)
	{
		unsigned newidx;

		if ((writeidx + 1) >= buffer.size())
			newidx = 0;
		else
		{
			newidx = writeidx + 1;
			//std::cout << newidx << std::endl;
		}

		if (newidx == readidx)
			throw exception();


		buffer[writeidx] = datum;

		writeidx = newidx;
	}

private:

	volatile unsigned  readidx, writeidx;
	const char* name;
	vector<T> buffer;
};

#endif /* LOCKFREEBUFFER_H_ */
